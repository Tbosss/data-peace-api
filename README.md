# Data Peace Api

# Requirements to run this project

# Python3.8, Virtualenv, Django
**For Ubuntu**
# Install Python3.8 then install virtualenv with python3 version

`pip install virtualenv -p python3`

# then create the virtual environment and activate it  and install the requiremenets.txt

`virtualenv venv`

# if you are using vnev from repo then simply activate this

`source venv/bin/activate`

`pip install -r requirements.txt`

# then run the server

`python manage.py runserver`