from django.contrib import admin
from .models import CustomUser
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# Register your models here.

class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None,{
        'fields':('email','password','first_name','last_name','company_name','age','city','state','zip','web','last_login')
    }),
    ('permission',{
        'fields':('is_active','is_staff','is_superuser','groups','user_permissions')
    }),
    )
    add_fieldsets = ((
        None,{
            'classes':('wide',),
            'fields':('email','password1','password2')

        }),
    )
    list_display = ('email','first_name','last_name', 'is_staff','last_login')
    list_filter = ('is_active','is_staff','is_superuser','groups')
    ordering = ('email',)
    search_fields = ('email',)
    filter_horizontal = ('groups','user_permissions',)


admin.site.register(CustomUser,UserAdmin)