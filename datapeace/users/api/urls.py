from django.urls import path
from . import views

urlpatterns = [
    path('users/',views.UserViewSet.as_view({'get': 'list','post':'create'})),
    path('users/<pk>/',views.UserViewSet.as_view({'get':'retrieve','put':'update','delete':'destroy'}))


]