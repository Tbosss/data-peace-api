from . import serializers
from rest_framework import views,viewsets
from users import models
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from .ordering import MyCustomOrdering
from rest_framework.settings import api_settings


ORDERING_PARAM= 'sort'

class UserViewSet(viewsets.ModelViewSet):
    serializer_class=serializers.UserSerializers
    queryset=models.CustomUser.objects.all()
    filter_backends=(DjangoFilterBackend,OrderingFilter)
    filter_fields=('first_name','last_name',)
    ordering_fields='__all__'

    # def get_queryset(self):
    #     queryset=models.CustomUser.objects.all()
    #     first_name=self.request.query_params.get('first_name')
    #     last_name=self.request.query_params.get('last_name')
    #     age=self.request.query_params.get('age')

        # if first_name:
        #     queryset = queryset.filter(first_name=first_name).order_by()
        # elif last_name:
        #     queryset = queryset.filter(last_name=last_name).order_by()
        # elif age:
        #     queryset = queryset.filter(age=age).order_by(   )

        # return queryset