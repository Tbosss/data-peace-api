from rest_framework import serializers
from users.models import CustomUser



class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model=CustomUser
        fields= ['id','first_name','last_name','company_name','age','city','state','zip','email','web']