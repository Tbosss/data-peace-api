from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser,BaseUserManager
from django.utils.translation import gettext as _
from django.utils import timezone
#from .managers import UserManager
# Create your models here.

class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        if not email:
            raise ValueError('user must have email address')
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            is_staff=is_staff,
            is_active=True,
            is_superuser=is_superuser,
            last_login=now,
            date_joined=now,
            **extra_fields
        )
        if password:
            user.set_password(password)
        user.save(using=self._db)
        return user
    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        user=self._create_user(email, password, True, True, **extra_fields)
        user.save(using=self._db)
        return user

class CustomUser(AbstractBaseUser,PermissionsMixin):
    email=models.EmailField(_('email address'),unique=True,blank=True)
    first_name=models.CharField(_('first name'),max_length=30, blank=True)
    last_name=models.CharField(_('last_name'), max_length=30,blank=True)
    company_name=models.CharField(_('company_name'),max_length=50,blank=True)
    city=models.CharField(_('city'),max_length=30,blank=True)
    state=models.CharField(_('state'),max_length=30,blank=True)
    zip=models.CharField(_('zip'),max_length=6,blank=True)
    web=models.URLField(_('web_url'), max_length=200,blank=True)
    age=models.CharField(max_length=50,blank=True)
    is_staff=models.BooleanField(default=False)
    is_superuser=models.BooleanField(default=False)
    is_active=models.BooleanField(default=True)
    last_login=models.DateTimeField(null=True,blank=True)
    date_joined=models.DateTimeField(auto_now_add=True)

    
    USERNAME_FIELD='email'
    EMAIL_FIELD='email'
    REQUIRED_FIELDS=[]
    objects=UserManager()
    

    def get_absolute_url(self):
        return "/users/%i/" % (self.pk)